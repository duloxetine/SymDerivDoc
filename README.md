# SymDerivDoc : Lispでやる記号微分スライド

## deriv.scmの実行
GaucheなどのSchemeインタプリタをインストールして、
```scheme
(load "./deriv.scm")
```
として下さい。
あとは好きなように微分ができます。

``` scheme
(deriv '(** x 2) 'x)
```
などなど。

## スライドのコンパイル
TeX Liveをインストールして、
```
latexmk -lualatex main.tex
```
とすれば、main.pdfができます。

フォントはNotoフォントを使っているので、
フォントのコンパイルエラーが出たら、

``` tex
\usepackage[noto-otf]{luatexja-preset}
```

の行を削除するか、`[noto-otf]`を削除するか、
Notoフォント(日本語)をインストールするかしてみて下さい。

Notoフォントは
https://www.google.com/get/noto/
から「JP」で検索して、NotoSansCJKjp-hinted.zipをダウンロードして
インストールしてみて下さい。
